# cloud-functions

> Shared cloud functions for use across our projects.

## API

### `parseSaveAll(collection = [])`

A promisified version of `Parse.Object.saveAll`.

### `success(response)`

A small utility method for calling `response.success()` on the object that was
passed in.

```js
const cloudFunctions = require('cloud-functions');
const { success } = cloudFunctions;

const beforeSave = (request, response) => {
  return Parse.Promise.as(request.object)
    .then(success(response));
}

module.exports = beforeSave;
```

### `createRelationship({ parent, column, temporaryField, object })`

Creates an array of pointers on the `column` of the `parent` class. With this
function, a Child can belong to many Parent classes; if uniqueness is required,
then see `createUniqueRelationship`.

This is an unwrapped method in order to enable more complex use cases, and must
be called manually from a Cloud function.

```js
const cloudFunctions = require('cloud-functions');
const { createRelationship, success } = cloudFunctions;

const afterSave = (request, response) => {
  return createRelationship({
    parent: 'Parent',   // The class that we're building the relationship to
    column: 'childArr', // The column that we're using for the relationship
    temporaryField: 'TEMP__parentId', // The temporary field to store the parent IDs on
    object: request.object, // The child that we want to build the relationship from
  })
    // Here, you can run any other methods for more advanced use cases;
    // make sure to call success once you are finished.
    .then(success(response));
}

module.exports = afterSave;
```

### `wrappedCreateRelationship({ parent, column, temporaryField })`

A wrapped method for simple use cases; this is equivalent to the above code.

```js
const cloudFunctions = require('cloud-functions');
const { wrappedCreateRelationship } = cloudFunctions;

const afterSave = wrappedCreateRelationship({
  parent: 'Parent',   // The class that we're building the relationship to
  column: 'childArr', // The column that we're using for the relationship
  temporaryField: 'TEMP__parentId', // The temporary field to store the parent IDs on
});

module.exports = afterSave;
```

### `createUniqueRelationship({ parent, column, temporaryField, object })`

Creates an array of pointers on the `column` of the `parent` class. With this
function, a Child can only belong to one Parent class; if uniqueness is not
required, then see `createRelationship`.

This is an unwrapped method in order to enable more complex use cases, and must
be called manually from a Cloud function.

```js
const cloudFunctions = require('cloud-functions');
const { createUniqueRelationship, success } = cloudFunctions;

const afterSave = (request, response) => {
  return createUniqueRelationship({
    parent: 'Parent',   // The class that we're building the relationship to
    column: 'childArr', // The column that we're using for the relationship
    temporaryField: 'TEMP__parentId', // The temporary field to store the parent IDs on
    object: request.object, // The child that we want to build the relationship from
  })
    // Here, you can run any other methods for more advanced use cases;
    // make sure to call success once you are finished.
    .then(success(response));
}

module.exports = afterSave;
```

### `wrappedCreateUniqueRelationship({ parent, column, temporaryField })`

A wrapped method for simple use cases; this is equivalent to the above code.

```js
const cloudFunctions = require('cloud-functions');
const { wrappedCreateUniqueRelationship } = cloudFunctions;

const afterSave = wrappedCreateUniqueRelationship({
  parent: 'Parent',   // The class that we're building the relationship to
  column: 'childArr', // The column that we're using for the relationship
  temporaryField: 'TEMP__parentId', // The temporary field to store the parent IDs on
});

module.exports = afterSave;
```

### `destroyRelationship({ parent, column, object })`

Destroys from an array of pointers on the `column` of the `parent` class before
a Child is deleted.

This is an unwrapped method in order to enable more complex use cases, and must
be called manually from a Cloud function.

```js
const cloudFunctions = require('cloud-functions');
const { destroyRelationship, success } = cloudFunctions;

const beforeDelete = (request, response) => {
  return destroyRelationship({
    parent: 'Parent',   // The class that we're destroying the relationship from
    column: 'childArr', // The column that we're using for the relationship
    object: request.object, // The child that is being deleted
  })
    // Here, you can run any other methods for more advanced use cases;
    // make sure to call success once you are finished.
    .then(success(response));
}

module.exports = beforeDelete;
```

### `wrappedDestroyRelationship({ parent, column })`

A wrapped method for simple use cases; this is equivalent to the above code.

```js
const cloudFunctions = require('cloud-functions');
const { wrappedDestroyRelationship, success } = cloudFunctions;

const beforeDelete = wrappedDestroyRelationship({
  parent: 'Parent',   // The class that we're destroying the relationship from
  column: 'childArr', // The column that we're using for the relationship
});

module.exports = beforeDelete;
```
