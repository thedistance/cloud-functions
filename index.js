/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2017-10-06T09:51:46+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2017-10-06T11:48:54+01:00
 * @Copyright: The Distance
 */

const createRelationship = require('./lib/createRelationship');
const createUniqueRelationship = require('./lib/createUniqueRelationship');
const destroyRelationship = require('./lib/destroyRelationship');
const parseSaveAll = require('./lib/parseSaveAll');
const success = require('./lib/success');
const wrappedCreateRelationship = require('./lib/wrappedCreateRelationship');
const wrappedCreateUniqueRelationship = require('./lib/wrappedCreateUniqueRelationship');
const wrappedDestroyRelationship = require('./lib/wrappedDestroyRelationship');

module.exports = {
  createRelationship,
  createUniqueRelationship,
  destroyRelationship,
  parseSaveAll,
  success,
  wrappedCreateRelationship,
  wrappedCreateUniqueRelationship,
  wrappedDestroyRelationship,
};
