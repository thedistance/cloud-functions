# 1.0.1

* Fix cloud function permissions.

# 1.0.0

* Initial release.
