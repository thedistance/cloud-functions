/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2017-10-06T10:48:54+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2017-10-06T10:56:08+01:00
 * @Copyright: The Distance
 */

const Parse = global.Parse = require('parse');
const ParseMockDB = require('parse-mockdb');
const destroyRelationship = require('./destroyRelationship');

const Parent = Parse.Object.extend('Parent');
const Child = Parse.Object.extend('Child');

beforeEach(() => {
  ParseMockDB.mockDB();
});

afterEach(() => {
  ParseMockDB.cleanUp();
  ParseMockDB.unMockDB();
});

test('should destroy a relationship from a single parent to a single child', () => {
  return new Child()
    .save()
    .then(child => Promise.all([
      new Parent({
        childArr: [ child.toPointer() ]
      }).save(),
      child
    ]))
    .then(([parent, child]) => destroyRelationship({
      parent: 'Parent',
      column: 'childArr',
      object: child,
    }))
    .then(() => Promise.all([
      new Parse.Query('Parent').first(),
      new Parse.Query('Child').first(),
    ]))
    .then(([parent, child]) => {
      expect(parent.get('childArr')).toHaveLength(0);
    });
});

test('should handle parents without an array of pointers', () => {
  return Promise.all([
    new Parent().save(),
    new Child().save(),
  ])
    .then(([parent, child]) => destroyRelationship({
      parent: 'Parent',
      column: 'childArr',
      object: child,
    }))
    .then(child => {
      expect(child).toBeInstanceOf(Child);
    });
});
