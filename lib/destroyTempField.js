/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2017-10-06T09:54:38+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-02-28T14:33:46+00:00
 * @Copyright: The Distance
 */

const destroyTempField = field => item => {
  // Destroy the temporary field when it's no longer needed
  item.unset(field);
  return item.save(null, { useMasterKey: true });
}

module.exports = destroyTempField;
