/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2017-10-06T10:39:19+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2017-10-06T10:41:50+01:00
 * @Copyright: The Distance
 */

const Parse = global.Parse = require('parse');
const ParseMockDB = require('parse-mockdb');
const wrappedCreateRelationship = require('./wrappedCreateRelationship');

const Parent = Parse.Object.extend('Parent');
const Child = Parse.Object.extend('Child');

beforeEach(() => {
  ParseMockDB.mockDB();
  ParseMockDB.registerHook('Child', 'afterSave', wrappedCreateRelationship({
    parent: 'Parent',
    column: 'childArr',
    temporaryField: 'TEMP__parentId',
  }));
});

afterEach(() => {
  ParseMockDB.cleanUp();
  ParseMockDB.unMockDB();
});

test('should run createRelationship on after save', () => {
  return new Parent()
    .save()
    .then(parent => new Child({ TEMP__parentId: [parent.id] }).save())
    .then(() => Promise.all([
      new Parse.Query('Parent').first(),
      new Parse.Query('Child').first(),
    ]))
    .then(([parent, child]) => {
      expect(child.get('TEMP__parentId')).toBeUndefined();
      expect(parent.get('childArr')).toHaveLength(1);
      expect(parent.get('childArr')[0]).toBeInstanceOf(Child);
    });
});
