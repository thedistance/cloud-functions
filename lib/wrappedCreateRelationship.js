/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2017-10-06T09:56:55+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2017-10-06T10:45:18+01:00
 * @Copyright: The Distance
 */

const success = require('./success');
const createRelationship = require('./createRelationship');

const wrappedCreateRelationship = ({ parent, column, temporaryField }) => (request, response) => {
  return createRelationship({
    parent,
    column,
    object: request.object,
    temporaryField,
  })
    .then(success(response))
    .catch((err) => response.error(err));
};

module.exports = wrappedCreateRelationship;
