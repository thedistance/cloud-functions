/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2017-10-06T10:57:07+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2017-10-06T11:01:25+01:00
 * @Copyright: The Distance
 */

const Parse = global.Parse = require('parse');
const ParseMockDB = require('parse-mockdb');
const wrappedDestroyRelationship = require('./wrappedDestroyRelationship');

const Parent = Parse.Object.extend('Parent');
const Child = Parse.Object.extend('Child');

beforeEach(() => {
  ParseMockDB.mockDB();
  ParseMockDB.registerHook('Child', 'beforeDelete', wrappedDestroyRelationship({
    parent: 'Parent',
    column: 'childArr',
  }));
});

afterEach(() => {
  ParseMockDB.cleanUp();
  ParseMockDB.unMockDB();
});

test('should run destroyRelationship on before delete', () => {
  return new Child()
    .save()
    .then(child => Promise.all([
      new Parent({
        childArr: [ child.toPointer() ]
      }).save(),
      child
    ]))
    .then(([parent, child]) => child.destroy())
    .then(() => Promise.all([
      new Parse.Query('Parent').first(),
      new Parse.Query('Child').first(),
    ]))
    .then(([parent, child]) => {
      expect(parent.get('childArr')).toHaveLength(0);
    });
});
