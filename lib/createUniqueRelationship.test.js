/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2017-10-06T11:07:17+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2017-10-06T11:26:41+01:00
 * @Copyright: The Distance
 */

const Parse = global.Parse = require('parse');
const ParseMockDB = require('parse-mockdb');
const createUniqueRelationship = require('./createUniqueRelationship');

const Parent = Parse.Object.extend('Parent');
const Child = Parse.Object.extend('Child');

beforeEach(() => {
  ParseMockDB.mockDB();
});

afterEach(() => {
  ParseMockDB.cleanUp();
  ParseMockDB.unMockDB();
});

test('should create a relationship from a single parent to a single child', () => {
  return new Parent()
    .save()
    .then(parent => new Child({ TEMP__parentId: parent.id }).save())
    .then(child => createUniqueRelationship({
      parent: 'Parent',
      column: 'childArr',
      object: child,
      temporaryField: 'TEMP__parentId',
    }))
    .then(() => Promise.all([
      new Parse.Query('Parent').first(),
      new Parse.Query('Child').first(),
    ]))
    .then(([parent, child]) => {
      expect(child.get('TEMP__parentId')).toBeUndefined();
      expect(parent.get('childArr')).toHaveLength(1);
      expect(parent.get('childArr')[0]).toBeInstanceOf(Child);
    });
});

test('should move a child from one parent to another', () => {
  return Promise.all([
    new Parent({ index: 1 }).save(),
    new Parent({ index: 2 }).save(),
  ])
    .then(([p1]) => new Child({ TEMP__parentId: p1.id }).save())
    .then(child => createUniqueRelationship({
      parent: 'Parent',
      column: 'childArr',
      object: child,
      temporaryField: 'TEMP__parentId',
    }))
    .then(child => Promise.all([
      new Parse.Query('Parent').equalTo('index', 1).first(),
      new Parse.Query('Parent').equalTo('index', 2).first(),
      child,
    ]))
    .then(([p1, p2, child]) => {
      expect(p1.get('childArr')).toHaveLength(1);
      expect(p2.get('childArr')).toBeUndefined();

      child.set('TEMP__parentId', p2.id);
      return child.save();
    })
    .then(child => createUniqueRelationship({
      parent: 'Parent',
      column: 'childArr',
      object: child,
      temporaryField: 'TEMP__parentId',
    }))
    .then(child => Promise.all([
      new Parse.Query('Parent').equalTo('index', 1).first(),
      new Parse.Query('Parent').equalTo('index', 2).first(),
      child,
    ]))
    .then(([p1, p2, child]) => {
      expect(p1.get('childArr')).toBeUndefined();
      expect(p2.get('childArr')).toHaveLength(1);
    });
  /*
  return new Parent()
    .save()
    .then(parent => new Child({ TEMP__parentId: parent.id }).save())
    .then(child => createUniqueRelationship({
      parent: 'Parent',
      column: 'childArr',
      object: child,
      temporaryField: 'TEMP__parentId',
    }))
    .then(() => Promise.all([
      new Parse.Query('Parent').first(),
      new Parse.Query('Child').first(),
    ]))
    .then(([parent, child]) => {
      expect(child.get('TEMP__parentId')).toBeUndefined();
      expect(parent.get('childArr')).toHaveLength(1);
      expect(parent.get('childArr')[0]).toBeInstanceOf(Child);
    }); */
});
