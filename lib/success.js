/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2017-10-06T09:56:06+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2017-10-06T09:56:41+01:00
 * @Copyright: The Distance
 */

const success = response => object => {
  // The response object is not available for in-memory tests.
  if (response) response.success(object);
  return object;
}

module.exports = success;
