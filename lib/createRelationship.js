/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2017-10-06T09:52:28+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-02-28T14:32:58+00:00
 * @Copyright: The Distance
 */

const destroyTempField = require('./destroyTempField');
const parseSaveAll = require('./parseSaveAll');

const createRelationship = ({ parent, column, object, temporaryField }) => {
  const parentIds = object.get(temporaryField);

  // If we didn't set the temp field, there's nothing to update.
  if (!parentIds) {
    return new Parse.Promise.resolve(object);
  }

  const pointer = object.toPointer();

  return new Parse.Query(parent)
    .limit(1000000)
    .find({ useMasterKey: true })
    .then(children => children.reduce((toUpdate, child) => {
      const pointerArr = child.get(column) || [];
      const pointerIds = pointerArr.map(p => p.id);
      if (parentIds.includes(child.id) && !pointerIds.includes(pointer.objectId)) {
        child.addUnique(column, pointer);
        toUpdate.push(child);
      }
      if (!parentIds.includes(child.id) && pointerIds.includes(pointer.objectId)) {
        child.remove(column, pointer);
        toUpdate.push(child);
      }
      return toUpdate;
    }, []))
    .then(parseSaveAll)
    .then(() => object)
    .then(destroyTempField(temporaryField));
}

module.exports = createRelationship;
