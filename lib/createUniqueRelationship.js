/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2017-10-06T11:05:16+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-02-28T14:33:31+00:00
 * @Copyright: The Distance
 */

const destroyTempField = require('./destroyTempField');
const parseSaveAll = require('./parseSaveAll');
const success = require('./success');

const createUniqueRelationship = ({ parent, column, object, temporaryField }) => {
  const parentId = object.get(temporaryField);

  // If we didn't set the temp field, there's nothing to update.
  if (!parentId) {
    return new Parse.Promise.resolve(object);
  }

  const pointer = object.toPointer();

  // Firstly, we need to check that the child already belongs to
  // a parent, and if it does then we need to remove it.

  return new Parse.Query(parent)
    .notEqualTo('objectId', parentId)
    .limit(1000000)
    .find({ useMasterKey: true })
    .then(children => children.reduce((toSave, child) => {
      const arr = child.get(column) || [];

      if (arr.map(t => t.id).includes(object.id)) {
        child.remove(column, pointer);
        toSave.push(child);
      }

      return toSave;
    }, []))
    .then(parseSaveAll)
    .then(children => children.map(child => {
      if (child.get(column).length < 1) {
        child.unset(column);
      }
      return child;
    }))
    .then(parseSaveAll)
    .then(() => new Parse.Query(parent).get(parentId, { useMasterKey: true }))
    .then(child => {
      child.addUnique(column, pointer);
      return child.save(null, { useMasterKey: true });
    })
    .then(() => object)
    .then(destroyTempField(temporaryField));
};

module.exports = createUniqueRelationship;
