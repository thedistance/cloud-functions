/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2017-10-06T10:43:50+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2017-10-06T10:48:36+01:00
 * @Copyright: The Distance
 */

const success = require('./success');
const destroyRelationship = require('./destroyRelationship');

const wrappedDestroyRelationship = ({ parent, column }) => (request, response) => {
  return destroyRelationship({
    parent,
    column,
    object: request.object,
  })
    .then(success(response))
    .catch((err) => response.error(err));
};

module.exports = wrappedDestroyRelationship;
