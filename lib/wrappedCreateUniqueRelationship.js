/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2017-10-06T11:32:42+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2017-10-06T11:57:59+01:00
 * @Copyright: The Distance
 */

const success = require('./success');
const createUniqueRelationship = require('./createUniqueRelationship');

const wrappedCreateUniqueRelationship = ({ parent, column, temporaryField }) => (request, response) => {
  return createUniqueRelationship({
    parent,
    column,
    object: request.object,
    temporaryField,
  })
    .then(success(response))
    .catch((err) => response.error(err));
};

module.exports = wrappedCreateUniqueRelationship;
