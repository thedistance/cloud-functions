/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2017-10-06T09:53:49+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-02-28T14:33:10+00:00
 * @Copyright: The Distance
 */

const parseSaveAll = (collection) => new Promise((resolve, reject) => {
  Parse.Object.saveAll(collection, {
    success: (list) => resolve(list),
    error: (err) => reject(err),
    useMasterKey: true,
  });
});

module.exports = parseSaveAll;
