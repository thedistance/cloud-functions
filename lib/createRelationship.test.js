/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2017-10-06T10:05:53+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2017-10-06T10:38:12+01:00
 * @Copyright: The Distance
 */

const Parse = global.Parse = require('parse');
const ParseMockDB = require('parse-mockdb');
const createRelationship = require('./createRelationship');

const Parent = Parse.Object.extend('Parent');
const Child = Parse.Object.extend('Child');

beforeEach(() => {
  ParseMockDB.mockDB();
});

afterEach(() => {
  ParseMockDB.cleanUp();
  ParseMockDB.unMockDB();
});

test('should create a relationship from a single parent to a single child', () => {
  return new Parent()
    .save()
    .then(parent => new Child({ TEMP__parentId: [parent.id] }).save())
    .then(child => createRelationship({
      parent: 'Parent',
      column: 'childArr',
      object: child,
      temporaryField: 'TEMP__parentId',
    }))
    .then(() => Promise.all([
      new Parse.Query('Parent').first(),
      new Parse.Query('Child').first(),
    ]))
    .then(([parent, child]) => {
      expect(child.get('TEMP__parentId')).toBeUndefined();
      expect(parent.get('childArr')).toHaveLength(1);
      expect(parent.get('childArr')[0]).toBeInstanceOf(Child);
    });
});

test('should create a relationship from multiple parents to a single child', () => {
  return Promise.all([
    new Parent().save(),
    new Parent().save(),
  ])
    .then(([p1, p2]) => new Child({ TEMP__parentId: [p1.id, p2.id] }).save())
    .then(child => createRelationship({
      parent: 'Parent',
      column: 'childArr',
      object: child,
      temporaryField: 'TEMP__parentId',
    }))
    .then(() => Promise.all([
      new Parse.Query('Parent').find(),
      new Parse.Query('Child').first(),
    ]))
    .then(([parents, child]) => {
      parents.forEach(parent => {
        expect(parent.get('childArr')).toHaveLength(1);
        expect(parent.get('childArr')[0]).toBeInstanceOf(Child);
      });
      expect(child.get('TEMP__parentId')).toBeUndefined();
    });
});

test('should delete a relationship from a single parent to a single child', () => {
  return new Parent()
    .save()
    .then(parent => new Child({ TEMP__parentId: [parent.id] }).save())
    .then(child => createRelationship({
      parent: 'Parent',
      column: 'childArr',
      object: child,
      temporaryField: 'TEMP__parentId',
    }))
    .then(() => Promise.all([
      new Parse.Query('Parent').first(),
      new Parse.Query('Child').first(),
    ]))
    .then(([parent, child]) => {
      expect(child.get('TEMP__parentId')).toBeUndefined();
      expect(parent.get('childArr')).toHaveLength(1);
      expect(parent.get('childArr')[0]).toBeInstanceOf(Child);

      child.set('TEMP__parentId', []);
      return child.save();
    })
    .then(child => createRelationship({
      parent: 'Parent',
      column: 'childArr',
      object: child,
      temporaryField: 'TEMP__parentId',
    }))
    .then(() => Promise.all([
      new Parse.Query('Parent').first(),
      new Parse.Query('Child').first(),
    ]))
    .then(([parent, child]) => {
      expect(child.get('TEMP__parentId')).toBeUndefined();
      expect(parent.get('childArr')).toHaveLength(0);
    });
});

test('should not create a relationship when the temporary field is undefined', () => {
  return new Child()
    .save()
    .then(child => createRelationship({
      parent: 'Parent',
      column: 'childArr',
      object: child,
      temporaryField: 'TEMP__parentId',
    }))
    .then(child => {
      expect(child.get('TEMP__parentId')).toBeUndefined();
    });
});
