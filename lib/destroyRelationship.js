/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2017-10-06T10:46:15+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2018-02-28T14:33:38+00:00
 * @Copyright: The Distance
 */

const parseSaveAll = require('./parseSaveAll');
const success = require('./success');

const destroyRelationship = ({ parent, column, object }) => {
  const pointer = object.toPointer();

  return new Parse.Query(parent)
    .limit(1000000)
    .find({ useMasterKey: true })
    .then(children => children.reduce((removeFrom, child) => {
      const list = child.get(column) || [];

      if (list.map(t => t.id).includes(object.id)) {
        child.remove(column, pointer);
        removeFrom.push(child);
      }

      return removeFrom;
    }, []))
    .then(parseSaveAll)
    .then(() => object);
}

module.exports = destroyRelationship;
