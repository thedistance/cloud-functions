/**
 * @Author: Ben Briggs <benbriggs>
 * @Date:   2017-10-06T11:49:03+01:00
 * @Email:  ben.briggs@thedistance.co.uk
 * @Last modified by:   benbriggs
 * @Last modified time: 2017-10-06T11:51:50+01:00
 * @Copyright: The Distance
 */

const cloudFunctions = require('./');

test('should export a list of functions', () => {
  const allFuncs = key => typeof cloudFunctions[key] === 'function';
  expect(Object.keys(cloudFunctions).every(allFuncs)).toBe(true);
});
